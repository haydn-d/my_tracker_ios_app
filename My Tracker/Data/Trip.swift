//
//  Trip.swift
//  My Tracker
//
//  Created by iOS Developer on 12/3/19.
//  Copyright © 2019 iOS Developer. All rights reserved.
//

import MapKit

struct Trip: Serializable {
    var locations: [Location]
}

struct Location : Serializable {
    let latitude: CLLocationDegrees
    let longitude: CLLocationDegrees
    let altitude: CLLocationDistance
    let horizontalAccuracy: CLLocationAccuracy
    let verticalAccuracy: CLLocationAccuracy
    let speed: CLLocationSpeed
    let course: CLLocationDirection
    let timestamp: Date
}
