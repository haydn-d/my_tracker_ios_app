//
//  Serializer.swift
//  My Tracker
//
//  Created by iOS Developer on 19/3/19.
//  Copyright © 2019 iOS Developer. All rights reserved.
//

import Foundation

protocol Serializable: Codable {
    init?(data: Data?)
    func encode() -> Data?
}

extension Serializable{
    init?(data: Data?){
        guard let data = data else{
            return nil
        }
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .useDefaultKeys
        self = try! decoder.decode(Self.self, from: data)
    }
    
    func encode() -> Data? {
        let encoder = JSONEncoder()
        return try? encoder.encode(self)
    }
}

