//
//  LocationManager.swift
//  My Tracker
//
//  Created by iOS Developer on 13/3/19.
//  Copyright © 2019 iOS Developer. All rights reserved.
//

import CoreLocation
import UIKit

class LocationManager: NSObject, CLLocationManagerDelegate {

    var locationManager = CLLocationManager()
    var currentViewController:UIViewController?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let locationDisabledAlert = UIAlertController(title: "Location Required", message: "This app reqiures location services to be always on", preferredStyle: .alert)
    var trackingInProgress = false
    //var locations = [CLLocation]()
    
    override init(){
        
        super.init()
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.showsBackgroundLocationIndicator = true
        

        locationDisabledAlert.addAction(UIAlertAction(title: "Open Settings", style: .default, handler:{
            action in switch action.style{
            default:
                if !CLLocationManager.locationServicesEnabled(){
                    if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION"){
                        UIApplication.shared.open(url)
                        self.currentViewController?.dismiss(animated: true, completion: nil)
                        return
                    }
                }
                if CLLocationManager.authorizationStatus() != .authorizedAlways{
                    if let url = URL(string: UIApplication.openSettingsURLString){
                        UIApplication.shared.open(url)
                        self.currentViewController?.dismiss(animated: true, completion: nil)
                        return
                    }
                }
            }
        }))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let viewController = appDelegate.window?.rootViewController
        checkLocationAuthStatus(viewController: viewController!)
        
    }
    
    func checkLocationAuthStatus(viewController:UIViewController){
        self.currentViewController = viewController
        if CLLocationManager.authorizationStatus() != .authorizedAlways{
            //if viewController.presentedViewController != nil{
            //    print("dismissing controller")
            
            //}
            viewController.present(locationDisabledAlert, animated: true, completion: nil)
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //self.topMostController()?.present(locationDisabledAlert, animated: true, completion: nil)
            //appDelegate.window?.rootViewController?.present(locationDisabledAlert, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //let viewController = appDelegate.window?.rootViewController
        //checkLocationAuthStatus(viewController: viewController!)  //note: doing a checklocation auth here duplicates triggers when using the enter foreground
        print("authchanged")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        print("location update received")
        for l in locations{
            print(l.coordinate)
            print(l.timestamp)
            print(l)
            //self.locations.append(l)
            
            let location = Location(latitude:l.coordinate.latitude, longitude:l.coordinate.longitude, altitude:l.altitude, horizontalAccuracy:l.horizontalAccuracy, verticalAccuracy:l.verticalAccuracy, speed:l.speed, course: l.course, timestamp:l.timestamp )
            
            self.appDelegate.trip?.locations.append(location)
            Disk.store(self.appDelegate.trip!, to: .documents, as: self.appDelegate.tripFile)
            
            NotificationCenter.default.post(name:Notification.Name("locationUpdateReceived"), object: nil)
            //NotificationCenter.default.post(name: Notification.Name("locationUpdateReceived"), object: l.coordinate)
        }
    }
    
    func appEnteredForegroud(){
        
        let viewController = self.appDelegate.window?.rootViewController
        self.checkLocationAuthStatus(viewController: viewController!)
    }
    
    func startLocationTracking(){
        locationManager.startUpdatingLocation()
        self.trackingInProgress = true
    }
    
    func stopLocationTracking(){
        locationManager.stopUpdatingLocation()
        self.trackingInProgress = false
    }
    
    func resetLocationTracking(){
        if self.trackingInProgress == true{
            locationManager.stopUpdatingLocation()
            self.trackingInProgress = false
        }
        
        self.appDelegate.trip?.locations = []
        Disk.store(self.appDelegate.trip!, to: .documents, as: self.appDelegate.tripFile)
        //self.locations = [CLLocation]()
    }
}
