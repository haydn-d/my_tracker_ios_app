//
//  AppDelegate.swift
//  My Tracker
//
//  Created by iOS Developer on 7/3/19.
//  Copyright © 2019 iOS Developer. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var locman: LocationManager?
    var trip: Trip?
    let tripFile:String = "currentTrip.txt"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //create trip if it doesnt exist
        
        
        //init location manager
        self.locman = LocationManager()

        //get current trip
        self.getCurrentTrip()
        print(self.trip!)
        
        print("app init")
        return true
        
    }
    
    func getCurrentTrip(){
        //get or init trip
        
        if Disk.fileExists(tripFile, in: .documents){
            print("trip exists")
            self.trip = Disk.retrieve(tripFile, from: .documents, as: Trip.self)
        }
        else{
            print("creating new trip")
            self.trip = Trip(locations:[])
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        print("app will resign active")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("app did enter background")
        self.window?.rootViewController!.dismiss(animated: true, completion: nil)    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("app will enter foreground")
        
        self.locman?.appEnteredForegroud()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("app did become active")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("app terminating")
    }
}
