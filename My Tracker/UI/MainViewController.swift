//
//  MainView.swift
//  My Tracker
//
//  Created by iOS Developer on 12/3/19.
//  Copyright © 2019 iOS Developer. All rights reserved.
//

import UIKit
import MapKit

class MainViewController: UIViewController, MKMapViewDelegate{
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var mapView: MKMapView!
    var polyline: MKPolyline? = nil
    var coordinatesList: [CLLocationCoordinate2D] = []

    override func viewDidLoad() {
        mapView.delegate = self

        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveLocationUpdate(_:)), name: NSNotification.Name("locationUpdateReceived"), object: nil)
    }
    
    @objc func onDidReceiveLocationUpdate(_ notification: Notification){
        print("view received location update")
        print(notification)
        
        var coordList: [CLLocationCoordinate2D] = []
        
        let trip = Disk.retrieve(self.appDelegate.tripFile, from: .documents, as: Trip.self)
        self.appDelegate.trip = trip
        
        for l in (self.appDelegate.trip?.locations)!{
            let coord = CLLocationCoordinate2D(latitude: l.latitude, longitude: l.longitude)
            coordList.append(coord)
        }
        
        /*
        self.coordinatesList.append(notification.object as! CLLocationCoordinate2D)
        print(self.coordinatesList)
        self.polyline = MKGeodesicPolyline(coordinates: self.coordinatesList, count: self.coordinatesList.count)
        */
        
        self.polyline = MKGeodesicPolyline(coordinates: coordList, count: coordList.count)
        
        self.mapView.removeOverlays(self.mapView.overlays)
        //self.mapView.removeOverlay(self.polyline!)
        self.mapView.addOverlay(self.polyline!)
        
        self.zoomToPolyline()
    }
    
    func zoomToPolyline(){
        self.mapView.setVisibleMapRect((self.polyline?.boundingMapRect)!, edgePadding:UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right:10.0), animated: true)
    }
    
    func mapView(_ mapView:MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer{
        
        print("renderer called")
        
        if let overlayGeodiesic = overlay as? MKGeodesicPolyline{
            print("geoid renderer called")
            let renderer = MKPolylineRenderer(polyline: overlayGeodiesic)
            renderer.strokeColor = UIColor.blue
            renderer.lineWidth = 5.0
            return renderer
        }
        
        return MKOverlayRenderer(overlay:overlay)
    
    }
    
    @IBOutlet weak var startTracking: UIButton!
    @IBOutlet weak var resetTracking: UIButton!
    @IBOutlet weak var stopTracking: UIButton!
    
    @IBAction func startTracking(_ sender: UIButton, forEvent event: UIEvent) {
        print("start tracking")
        self.startTracking.isEnabled = false
        self.stopTracking.isEnabled = true
        self.resetTracking.isEnabled = false
        appDelegate.locman?.startLocationTracking()
    }
    
    @IBAction func stopTracking(_ sender: UIButton, forEvent event: UIEvent) {
        print("stop tracking")
        self.startTracking.isEnabled = true
        self.stopTracking.isEnabled = false
        self.resetTracking.isEnabled = true
        appDelegate.locman?.stopLocationTracking()
    }
    
    @IBAction func resetTracking(_ sender: Any, forEvent event: UIEvent) {
        print("reset tracking")
        self.startTracking.isEnabled = true
        self.stopTracking.isEnabled = false
        self.resetTracking.isEnabled = false
        appDelegate.locman?.resetLocationTracking()
        //self.coordinatesList = []

        self.mapView.removeOverlays(self.mapView.overlays)
        //self.mapView.removeOverlay(self.polyline!)
    }
}
